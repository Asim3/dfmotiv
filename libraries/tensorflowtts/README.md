
# Speech Inference Instructions #
---
## Requirements ##

- Python 3.7+ (we used 3.7.9)
- Cuda 10.1
- CuDNN 7.6.5

- clone this repo

- install requirements

```
#!

pip install TensorFlowTTS
```
This command will install the TensorflowTTS repo and its requirements. It is recommended that you create a new virtual environment before doing this. 

Trained models are not stored in this repo due to size. For pretrained text2mel model see [here](https://drive.google.com/drive/folders/1Q7QrTMksI-5F3_44-68ex2RkOO_789TW) and for pretrained vocoder see [here](https://drive.google.com/drive/folders/1ne89z914YHdlZHfcRLOAQHuzpRC3c9h-) (generator-940000). Note these are not Morgan Freeman models. If you want Morgan Freeman speech please use dc-tts. A morgan Freeman Text2Mel model requires a larger dataset and more development. The vocoder is universal and can theoretically be applied to any text2mel model, however fine-tuning will always return better results.
- Place trained models into models/download
- Edit script.txt
What you type in here will be converted to speech

- run ljspeech_inference.py

```
#!

python3 inference.py
```

`libritts_inference.py` is another inference module that uses models trained on multi-speaker dataset including the target celebrity. At inference time it uses speaker encodings to inference that speaker's speech. This requires further development. If you are interested, please see [this](https://github.com/TensorSpeech/TensorFlowTTS/issues/296) thread.
