"""
Module to convert text to speech based on our own training models
ARGS:
    fastspeech2_config: config file of fastspeech2 model
    fastspeech2: text-to-mel spectrogram model
    mb_melgan_config: config file of MB-Melgan model
    mb_melgan: vocoder model
    input_text (str): text to be converted to speech
"""

import numpy as np
import soundfile as sf
import yaml

import tensorflow as tf

from tensorflow_tts.inference import AutoConfig
from tensorflow_tts.inference import TFAutoModel
from tensorflow_tts.inference import AutoProcessor

from g2p_en import G2p

"""Open and read from script.txt"""
try:
    script = open("script.txt", "r")
    input_text = script.read()
except (FileNotFoundError):
    print("script.txt could not be opened. Please check that file is in root dir")

if input_text == "":
    print("script.txt is empty. Please add text and try again")
    quit()


"""need to download model files - not stored in repo due to size"""

# initialize fastspeech2 model
fastspeech2_config = AutoConfig.from_pretrained('./config_files/download/fastspeech2.v1.yaml')
fastspeech2 = TFAutoModel.from_pretrained(
    config=fastspeech2_config,
    pretrained_path="./models/download/model-150000.h5",
    name="fastspeech2"
)

# initialize MB-melgan model
mb_melgan_config = AutoConfig.from_pretrained('./config_files/multiband_melgan.v1.yaml')
mb_melgan = TFAutoModel.from_pretrained(
    config=mb_melgan_config,
    pretrained_path="./models/download/generator-920000.h5",
    # update model name
    name="mb_melgan"
)

# initialise text processor that converts test to a sequence of IDs
processor = AutoProcessor.from_pretrained(pretrained_path="./config_files/ljspeech_mapper.json")

# function converts text to a list of phonemes and then a sequence of phoneme IDs
# not used in ljspeech_inference.py as model is character based and not phoneme based
def get_input_phoneme_ids(input_txt):
    g2p = G2p()
    input_phoneme_list = g2p(input_txt)
    input_phonemes = ' '.join(input_phoneme_list)
    input_phonemes = ' '.join(input_phonemes.split())
    return processor.text_to_sequence(input_phonemes)

"""
this function synthesises the speech
    ARGS:
        input_text (str): script text
        text2mel_model: fastspeech2
        vocoder_model: mb_melgan
        text2mel_name (str): FASTSPEECH2
        vocoder_name (str): MB-MELGAN
 """
def do_synthesis(input_text, text2mel_model, vocoder_model, text2mel_name, vocoder_name):
    input_ids = processor.text_to_sequence(input_text)
    print(input_text)
    # text2mel part
    if text2mel_name == "TACOTRON":
        _, mel_outputs, stop_token_prediction, alignment_history = text2mel_model.inference(
            tf.expand_dims(tf.convert_to_tensor(input_ids, dtype=tf.int32), 0),
            tf.convert_to_tensor([len(input_ids)], tf.int32),
            tf.convert_to_tensor([0], dtype=tf.int32)
        )
    elif text2mel_name == "FASTSPEECH":
        mel_before, mel_outputs, duration_outputs = text2mel_model.inference(
            input_ids=tf.expand_dims(tf.convert_to_tensor(input_ids, dtype=tf.int32), 0),
            speaker_ids=tf.convert_to_tensor([0], dtype=tf.int32),
            speed_ratios=tf.convert_to_tensor([1.0], dtype=tf.float32),
        )
    elif text2mel_name == "FASTSPEECH2":
        mel_before, mel_outputs, duration_outputs, _, _ = text2mel_model.inference(
            tf.expand_dims(tf.convert_to_tensor(input_ids, dtype=tf.int32), 0),
            speaker_ids=tf.convert_to_tensor([0], dtype=tf.int32), 
            speed_ratios=tf.convert_to_tensor([1.0], dtype=tf.float32),
            f0_ratios=tf.convert_to_tensor([1.0], dtype=tf.float32),
            energy_ratios=tf.convert_to_tensor([1.0], dtype=tf.float32),
        )
    else:
        raise ValueError("Only TACOTRON, FASTSPEECH, FASTSPEECH2 are supported on text2mel_name")

    # vocoder part
    if vocoder_name == "MELGAN" or vocoder_name == "MELGAN-STFT":
        audio = vocoder_model(mel_outputs)[0, :, 0]
    elif vocoder_name == "MB-MELGAN":
        audio = vocoder_model(mel_outputs)[0, :, 0]
    else:
        raise ValueError("Only MELGAN, MELGAN-STFT and MB_MELGAN are supported on vocoder_name")

    if text2mel_name == "TACOTRON":
        return mel_outputs.numpy(), alignment_history.numpy(), audio.numpy()
    else:
        return mel_outputs.numpy(), audio.numpy()

mels, audios = do_synthesis(input_text, fastspeech2, mb_melgan, "FASTSPEECH2", "MB-MELGAN")

# save to file
sf.write('./audio_out.wav', audios, 24000, "PCM_16")

